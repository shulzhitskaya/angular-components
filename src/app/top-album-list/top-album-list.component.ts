import { Component, OnInit, Input } from '@angular/core';

import { UserService } from '../services/user.service';
import {UserInfo} from "../classes/user-info";

@Component({
    selector: 'app-top-album-list',
    templateUrl: './top-album-list.component.html',
    styleUrls: ['./top-album-list.component.css']
})
export class TopAlbumListComponent implements OnInit {

    topAlbums:Array<any>;
    @Input() positionList:string = 'vertical';

    constructor(private userService:UserService) {
    }

    ngOnInit() {
        this.getTopAlbums();
    }

    getTopAlbums():void {
        this.userService.getTopAlbums().then(topAlbums => {
            this.topAlbums = topAlbums['album'].map(function(item){
                return {
                    title: item.name,
                    subtitle: item.playcount,
                    image: item.image[2]['#text']
                }
            });
        })
    }

}

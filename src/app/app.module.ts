import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';

import { UserService } from '../app/services/user.service';


import { NavigatePanelModule } from './navigate-panel/navigate-panel.module';
import { HomePageModule } from './home-page/home-page.module';
import { SharedModule } from "./shared/shared.module";

@NgModule({
    declarations: [
        AppComponent,
        /*TopArtistListComponent,
         TopAlbumListComponent*/
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        NgbModule.forRoot(),
        NavigatePanelModule,
      HomePageModule,
      SharedModule
    ],
    providers: [UserService],
    bootstrap: [AppComponent]
})
export class AppModule {
}

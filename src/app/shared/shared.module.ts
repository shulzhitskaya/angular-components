import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { VirtualScrollModule } from './virtual-scroll/virtual-scroll.module';
import { ChartListComponent } from './chart-list/chart-list.component';
import { HeartButtonComponent } from './heart-button/heart-button.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        CommonModule,
        VirtualScrollModule,
        NgbModule
    ],
    declarations: [
        ChartListComponent,
        HeartButtonComponent
    ],
    exports: [
        VirtualScrollModule,
        ChartListComponent,
        HeartButtonComponent
    ]
})
export class SharedModule {
}

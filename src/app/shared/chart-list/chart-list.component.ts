import { Component, OnInit, Input } from '@angular/core';

import { UserService } from '../../services/user.service';

@Component({
    selector: 'app-chart-list',
    templateUrl: './chart-list.component.html',
    styleUrls: ['./chart-list.component.css']
})
export class ChartListComponent implements OnInit {

    @Input() chartItemAmount:string;
    chartList:Array<any>;

    constructor(private userService:UserService) {
    }

    ngOnInit() {
        this.getRecentChart();
    }

    getRecentChart(): void{
        this.userService.getRecentTracks().then(tracks =>{
            this.chartList = tracks;
        });
    }
}

import { Component, OnInit, ElementRef } from '@angular/core';

@Component({
    selector: 'app-heart-button',
    templateUrl: './heart-button.component.html',
    styleUrls: ['./heart-button.component.css']
})
export class HeartButtonComponent implements OnInit {

    heartClasses:string = 'star empty-heart';
    currentIcon:string = 'empty-heart';

    constructor(private element:ElementRef) {
    }

    ngOnInit() {
    }

    changeStatus(event):void {
        this.currentIcon = this.currentIcon === 'empty-heart' ? 'full-heart' : 'empty-heart';
        this.heartClasses = this.currentIcon;
    }

    focusColor():void {
        this.heartClasses = ' focus ' + this.currentIcon;
    }

    leaveFocus():void {
        this.heartClasses = this.currentIcon;
    }

}

export class Dimension {
    containerWidth:number;
    containerHeight:number;
    childWidth:number;
    childHeight:number;
    amountVerticalItems:number;
    amountHorizontalItems: number;
}
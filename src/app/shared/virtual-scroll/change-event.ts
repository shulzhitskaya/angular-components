export interface ChangeEvent {
    start?: number;
    end?: number;
}

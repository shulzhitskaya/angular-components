import {
    Component,
    ElementRef,
    EventEmitter,
    Input,
    NgModule,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    Renderer,
    SimpleChanges,
    ViewChild,
} from '@angular/core';

import { Dimension } from './dimension'
import { ChangeEvent } from './change-event'

@Component({
    selector: 'app-virtual-scroll',
    templateUrl: './virtual-scroll.component.html',
    styleUrls: ['./virtual-scroll.component.css']
})
export class VirtualScrollComponent implements OnInit, OnDestroy, OnChanges {

    @Input() items:any[] = [];
    @Input() childWidth:number;
    @Input() childHeight:number;
    @Input() positionList:string = 'vertical';
    @Output()  update:EventEmitter<any[]> = new EventEmitter<any[]>();
    @ViewChild('content', {read: ElementRef}) contentElementRef:ElementRef;

    onScrollListener:Function;
    topPadding:number;
    leftPadding:number;
    scrollVertical:number;
    scrollHorizontal:number;
    previousStart:number;
    previousEnd:number;
    dimension:Dimension;
    scrollSize:{};
    scrollabletransform:{};
    firstRender:boolean = true;
    horizontalScrollWidth:number;
    verticalScrollHeight:number;


    constructor(private element:ElementRef, private renderer:Renderer) {
    }

    ngOnInit() {
        this.onScrollListener = this.renderer.listen(this.element.nativeElement, 'scroll', this.refresh.bind(this));

    }

    ngOnChanges(changes:SimpleChanges) {
        this.previousStart = undefined;
        this.previousEnd = undefined;

        this.refresh();
    }

    ngOnDestroy() {
        if (this.onScrollListener !== undefined) {
            // this removes the listener
            this.onScrollListener();
        }
    }


    ngAfterContentChecked() {
        if (this.previousStart === 0 && this.contentElementRef.nativeElement.children.length > 0 && this.firstRender === true) {
            this.refresh();
            this.firstRender = false;
        }
    }

    setScrollSize() {
        this.scrollSize = {
            'height': (this.scrollVertical || 1) + 'px',
            'width': (this.scrollHorizontal || 1) + 'px'
        };
    }

    getHorizontalScrollContent() {
        return this.horizontalScrollWidth;
    }

    getVerticalScrollHeight() {
        return this.verticalScrollHeight;
    }

    setScrollabletransform() {
        if (this.positionList === 'horizontal') {
            this.scrollabletransform = {
                'transform': 'translateX(' + (this.leftPadding || 0) + 'px)',
                'width': this.getHorizontalScrollContent() + 'px'
            };
        } else {
            this.scrollabletransform = {
                'transform': 'translateY(' + (this.topPadding || 0) + 'px)'
            };
        }
    }

    private setDimensions():Dimension {
        let element = this.element.nativeElement;
        let content = this.contentElementRef.nativeElement;

        let containerWidth = element.clientWidth;
        let containerHeight = element.clientHeight;

        let contentDimensions;

        if ((!this.childWidth || !this.childHeight)) {
            contentDimensions =
                (content.children.length > 0 ?
                    content.children[0].getBoundingClientRect() :
                {
                    width: containerWidth,
                    height: containerHeight
                });

            if (content.children.length === 0) {
                this.firstRender = true;
            }
        }

        let childWidth = this.childWidth || contentDimensions.width;
        let childHeight = this.childHeight || contentDimensions.height;

        let amountVerticalItems = Math.round(containerHeight / childHeight);
        let amountHorizontalItems = Math.floor(containerWidth / childWidth);

        return {
            containerWidth: containerWidth,
            containerHeight: containerHeight,
            childWidth: childWidth,
            childHeight: childHeight,
            amountVerticalItems: amountVerticalItems,
            amountHorizontalItems: amountHorizontalItems
        } as Dimension;
    }

    private getScrollHeight() {
        return this.scrollVertical || this.getDimensions().childHeight * this.items.length;
    }

    private getDimensions() {
        let dimension = this.dimension;
        if (!dimension) {
            dimension = this.setDimensions();
        }
        return dimension;
    }

    private calculateVisibleItems() {
        let self = this;
        let start, end, element, items, dimension, elementScrollLen, childAmount, childSize, scrollLen, scrollWidth, padding;

        element = self.element.nativeElement;
        items = self.items || [];

        if (items.length === 0) {
            return;
        }

        dimension = self.getDimensions();
        elementScrollLen = (self.positionList === 'vertical' ? element.scrollTop : element.scrollLeft);
        childAmount = (self.positionList === 'vertical' ? dimension.amountVerticalItems + 1 : dimension.amountHorizontalItems);
        childSize = (self.positionList === 'vertical' ? dimension.childHeight : dimension.childWidth);
        scrollLen = childSize * items.length;

        if (self.positionList === 'vertical') {
            self.scrollVertical = scrollLen;
        } else {
            self.scrollHorizontal = scrollLen;
        }

        if (elementScrollLen > scrollLen) {
            element = scrollLen;
        }

        start = Math.round(elementScrollLen / childSize);
        end = start + childAmount;

        if (self.previousStart !== start || (self.firstRender === false && self.previousStart === 0)) {
            if (start > 0) {
                start -= (1 + (start > 1 ? 1 : 0));
            }

            if (end != items.length && self.positionList === 'horizontal') {
                end += 1;
            }

            scrollWidth = (end - start) * childSize;

            if (this.positionList === 'vertical') {
                self.verticalScrollHeight = scrollWidth;
            } else {
                self.horizontalScrollWidth = scrollWidth;
            }

            self.update.emit(items.slice(start, end));
            self.previousStart = start;
            self.previousEnd = end;

            padding = childSize * start;

            if (self.positionList === 'vertical') {
                self.topPadding = padding;
            } else {
                self.leftPadding = padding;
            }
        }

        self.setScrollSize();
        self.setScrollabletransform();
    }

    private refresh() {
        requestAnimationFrame(this.calculateVisibleItems.bind(this));
    }
}


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TabItemDirective } from './directives/tab-item.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    TabItemDirective
  ],
  exports:[
    TabItemDirective
  ]
})
export class CoreModule { }

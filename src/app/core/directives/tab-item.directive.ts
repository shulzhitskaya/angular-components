import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[appTabItem]'
})
export class TabItemDirective {

  @Input('appTabItem') itemName: string;

  constructor(el:ElementRef) {
  }

  @HostListener('click', ['$event.target']) onClickItem(btn) {
    this.selectTab(this.itemName || '');
  }

  private selectTab(btn):void {
    console.log(btn);
  }
}

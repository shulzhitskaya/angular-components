import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopAlbumListComponent } from './top-album-list.component';

describe('TopAlbumListComponent', () => {
  let component: TopAlbumListComponent;
  let fixture: ComponentFixture<TopAlbumListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopAlbumListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopAlbumListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';

import { UserService } from '../services/user.service';
import { UserInfo } from '../classes/user-info';


@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  userImageUrl:string;
  userName:string;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getUserInfo();
  }

  getUserInfo():void {
    this.userService.getInfo().then(userInfo=> {
      this.userImageUrl = userInfo["image"][1]["#text"];
      this.userName = userInfo.name;

    });
  }

}

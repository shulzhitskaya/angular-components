import { Component, OnInit, Input } from '@angular/core';

import { UserService } from "../../services/user.service";
import { cardItem } from '../../classes/cardItem';
import { ChangeEvent } from "../../shared/virtual-scroll/change-event";

@Component({
    selector: 'app-top-artist-list',
    templateUrl: './top-artist-list.component.html',
    styleUrls: ['./top-artist-list.component.css']
})
export class TopArtistListComponent implements OnInit {

    topArtists:Array<cardItem>;
    @Input() positionList:string;

    constructor(private userService:UserService) {
    }

    ngOnInit() {
        this.getTopArtists();
    }

    getTopArtists():void {
        this.userService.getTopArtists().then(topArtists => {
            this.topArtists = topArtists['artist'].map(function(item){
                return {
                    title: item.name,
                    subtitle: item.playcount,
                    image: item.image[2]['#text']
                }
            });
        });
    }

}

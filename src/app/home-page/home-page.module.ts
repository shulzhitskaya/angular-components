import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomePageComponent } from './home-page.component';
import { SharedHomePageModule } from './shared/shared.module';
import { SharedModule } from '../shared/shared.module';

import { TopAlbumListComponent } from './top-album-list/top-album-list.component';
import { TopArtistListComponent } from './top-artist-list/top-artist-list.component';

@NgModule({
  imports: [
    CommonModule,
    SharedHomePageModule,
    SharedModule
  ],
  declarations: [
    HomePageComponent,
    TopAlbumListComponent,
    TopArtistListComponent
  ],
  exports:[
    HomePageComponent,
    SharedModule
  ]
})
export class HomePageModule { }

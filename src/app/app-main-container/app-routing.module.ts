import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import {TopAlbumListComponent} from '../top-album-list/top-album-list.component';
import {TopArtistListComponent} from '../home-page/top-artist-list/top-artist-list.component';


const routes:Routes = [
    {path: '', redirectTo: '/album', pathMatch: 'full'},
    {path: 'album', component: TopAlbumListComponent},
    {path: 'artist', component: TopArtistListComponent}
];


@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

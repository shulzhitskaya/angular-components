import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';

import { SharedModule } from '../shared/shared.module';
import {TopAlbumListComponent} from '../top-album-list/top-album-list.component';
import {TopArtistListComponent} from '../home-page/top-artist-list/top-artist-list.component';

import { AppRoutingModule } from './app-routing.module';
import {AppMainContainerComponent} from './app-main-container.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule
  ],
  declarations: [
    AppMainContainerComponent,
    TopAlbumListComponent,
    TopArtistListComponent
  ],
  exports:[
    AppMainContainerComponent,
    AppRoutingModule
  ]
})
export class AppMainModule { }

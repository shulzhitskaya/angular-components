import { ImagesInfo } from './images-info';
import { RegisteredInfo } from './registered-info';

export class UserInfo {
    "name":string;
    "image":Array<ImagesInfo>;
    "url":string;
    "country":string;
    "age":number;
    "gender":string;
    "subscriber":number;
    "playcount":number;
    "playlists":number;
    "bootstrap":number;
    "registered":RegisteredInfo;
    "type":string;
}

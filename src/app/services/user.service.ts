import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { UserInfo } from  '../classes/user-info';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {
    private headers = new Headers({'Content-Type': 'application/json'});
    private userUrl = 'http://localhost:4200/src/data/user';

    constructor(private http:Http) {
    }

    private handleError(error:any):Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    getInfo():Promise<UserInfo> {
        const url = `${this.userUrl}/getinfo.json`;
        return this.http.get(url)
            .toPromise()
            .then(responce => responce.json().user as UserInfo)
            .catch(this.handleError);
    }

    getTopArtists():Promise<Array<any>> {
        const url = `${this.userUrl}/getTopArtists.json`;
        return this.http.get(url)
            .toPromise()
            .then(responce => responce.json().topartists as Array<any>)
            .catch(this.handleError);
    }

    getTopAlbums():Promise<Array<any>> {
        const url = `${this.userUrl}/getTopAlbums.json`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json().topalbums as Array<any>)
            .catch(this.handleError);
    }

    getRecentTracks():Promise<Array<any>> {
        const url = `${this.userUrl}/getRecentTracks.json`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json().recenttracks.track as Array<any>)
            .catch(this.handleError);
    }

    getTopTracks():Promise<Array<any>> {
        const url = `${this.userUrl}/getTopTracks.json`;
        return this.http.get(url)
            .toPromise()
            .then(response => response.json().toptracks.track as Array<any>)
            .catch(this.handleError);
    }
}


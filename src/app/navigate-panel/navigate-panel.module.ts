import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import  {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';

import { NavigateRoutingModule } from './navigate-routing.module';
import { NavigatePanelComponent } from './navigate-panel.component';
import { HomePageModule } from '../home-page/home-page.module';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NavigateRoutingModule,
    RouterModule,
    HomePageModule,
    NgbModule
  ],
  declarations: [
    NavigatePanelComponent
  ],
  exports:[
    NavigatePanelComponent,
    NavigateRoutingModule

  ]
})
export class NavigatePanelModule { }

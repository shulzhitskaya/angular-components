import { Component, OnInit } from '@angular/core';

import { UserService } from '../services/user.service';
import { UserInfo } from '../classes/user-info';


@Component({
    selector: 'app-navigate-panel',
    templateUrl: './navigate-panel.component.html',
    styleUrls: ['./navigate-panel.component.css']
})
export class NavigatePanelComponent implements OnInit {

    userImageUrl:string;
    userName:string;
    userImageUrlSmall:string;
    userInfo:UserInfo;
    topTrack:Object = {};
    coverStyles:Object = {};
    actionList: Array<any> =[
      { title:'Home', route:'/home'},
      { title:'Loved tracks', route:'/lovedTracks'}
    ];
    selectedItem :Object={};
    actionClass:Object={};

    constructor(private userService:UserService) {
    }

    ngOnInit() {
        this.getUserInfo();
        this.getTopSong();
    }

    onSelectAction(event, action): void{
        this.selectedItem = action;
    }
    setCoverStyles(imageUrl):void {
        this.coverStyles = {
            'background-image': `url(${imageUrl})`
        };
    }

    getUserInfo():void {
        this.userService.getInfo().then(userInfo=> {
            this.userImageUrl = userInfo["image"][1]["#text"];
            this.userName = userInfo.name;
            this.userImageUrlSmall = userInfo["image"][0]["#text"];

            this.userInfo = userInfo;
        });
    }

    getTopSong():void {
        this.userService.getTopTracks().then(topTracks=> {
            this.topTrack = topTracks[0];
            let imageUrl = topTracks[0].image[3]['#text'];
            this.setCoverStyles(imageUrl);

        });
    }


}

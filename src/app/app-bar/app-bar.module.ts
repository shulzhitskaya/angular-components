import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppBarComponent } from './app-bar.component';
import { CoreModule } from '../core/core.module';

@NgModule({
    imports: [
        CommonModule,
        NgbModule,
        CoreModule
    ],
    declarations: [
        AppBarComponent
    ],
    exports:[
        AppBarComponent
    ]
})
export class AppBarModule {
}

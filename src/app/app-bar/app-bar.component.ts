import { Component, OnInit } from '@angular/core';

import { UserService } from '../services/user.service';
import { UserInfo } from '../classes/user-info';

@Component({
    selector: 'app-app-bar',
    templateUrl: './app-bar.component.html',
    styleUrls: ['./app-bar.component.css']
})
export class AppBarComponent implements OnInit {

    userImageUrl:string;
    userName:string;

    constructor(private userService:UserService) {
    }

    ngOnInit():void {
        this.getUserInfo();
    }

    getUserInfo():void {
        this.userService.getInfo().then(userInfo=> {
            this.userImageUrl = userInfo["image"][1]["#text"];
            this.userName = userInfo.name;

        });
    }

}

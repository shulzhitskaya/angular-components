import { LastFmPage } from './app.po';

describe('last-fm App', function() {
  let page: LastFmPage;

  beforeEach(() => {
    page = new LastFmPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
